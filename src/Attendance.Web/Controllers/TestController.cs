﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Attendance.Domain.Entities;
using Attendance.Repository.Repositories;
using Attendance.Repository.Interfaces;
using Attendance.Service.Interfaces;
using Attendance.Service.Services;
using Attendance.Web.Models;
using Attendance.Repository.EF;
using Microsoft.EntityFrameworkCore;


namespace Attendance.Web.Controllers
{
    public class TestController : Controller
    {
        private readonly IAdminService adminService;
        private readonly IGroupService groupService;

        public TestController(IAdminService adminService, IGroupService groupService)
        {
            this.adminService = adminService;
            this.groupService = groupService;
            
        }

        [HttpGet]
        public IActionResult AdminsList()
        {

            List<Admin> list = adminService.GetAdmins().ToList();

            List<AdminViewModel> model = new List<AdminViewModel>();


            foreach (var admin in list)
            {
                AdminViewModel adminModel = new AdminViewModel()
                {
                    Id = admin.Id,
                    Login = admin.Login,
                    Password = admin.Password
                };

                model.Add(adminModel);
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult GroupsList()
        {

            List<Group> list = groupService.GetGroups().ToList();

            List<GroupViewModel> model = new List<GroupViewModel>();


            foreach (var group in list)
            {
                GroupViewModel groupModel = new GroupViewModel()
                {
                    Id = group.Id,   
                };

                model.Add(groupModel);
            }

            return View(model);
        }


        [HttpPost]
        public IActionResult AddAdmin(string username, string password)
        {
            Admin admin = new Admin() {
                Id = Guid.NewGuid(),
                Login = username,
                Password = password
            };

            adminService.CreateAdmin(admin);

            if (username != null)
            {
                return RedirectToAction("AdminsList");
            }

            return View();
        }

        [HttpPost]
        public IActionResult DeleteAdmin(Admin admin)
        {

            adminService.DeleteAdmin(admin);

            return RedirectToAction("AdminsList");
        }


    }
}
