﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Attendance.Repository.EF;

namespace Attendance.Repository.Migrations
{
    [DbContext(typeof(AtendanceContext))]
    [Migration("20170305094532_StudentPresence")]
    partial class StudentPresence
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Attendance.Domain.Entities.Admin", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Login");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.ToTable("Admins");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Course", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<char>("CourseType");

                    b.Property<string>("SubjectId");

                    b.Property<string>("TeacherId");

                    b.Property<Guid?>("TeacherId1");

                    b.HasKey("Id");

                    b.HasIndex("SubjectId");

                    b.HasIndex("TeacherId1");

                    b.ToTable("Courses");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.CourseGroup", b =>
                {
                    b.Property<Guid>("CourseId");

                    b.Property<string>("GroupId");

                    b.HasKey("CourseId", "GroupId");

                    b.HasIndex("CourseId");

                    b.HasIndex("GroupId");

                    b.ToTable("CourseGroup");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Group", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Groups");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Presence", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CourseId");

                    b.Property<DateTime>("Date");

                    b.Property<int>("LessonNumber");

                    b.Property<string>("Status");

                    b.HasKey("Id");

                    b.HasIndex("CourseId");

                    b.ToTable("Presences");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Student", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("FirstName");

                    b.Property<string>("GroupId");

                    b.Property<string>("Lastname");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.ToTable("Students");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.StudentPresence", b =>
                {
                    b.Property<string>("StudentId");

                    b.Property<Guid>("PresenceId");

                    b.HasKey("StudentId", "PresenceId");

                    b.HasIndex("PresenceId");

                    b.HasIndex("StudentId");

                    b.ToTable("StudentPresence");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Subject", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("Eap");

                    b.Property<int>("Lesson");

                    b.Property<string>("Name");

                    b.Property<int>("Practice");

                    b.HasKey("Id");

                    b.ToTable("Subjects");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Teacher", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FirstName");

                    b.Property<string>("Lastname");

                    b.Property<string>("Login");

                    b.Property<string>("Pasword");

                    b.HasKey("Id");

                    b.ToTable("Teachers");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Course", b =>
                {
                    b.HasOne("Attendance.Domain.Entities.Subject", "Subject")
                        .WithMany("Courses")
                        .HasForeignKey("SubjectId");

                    b.HasOne("Attendance.Domain.Entities.Teacher", "Teacher")
                        .WithMany("Courses")
                        .HasForeignKey("TeacherId1");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.CourseGroup", b =>
                {
                    b.HasOne("Attendance.Domain.Entities.Course", "Course")
                        .WithMany("CourseGroups")
                        .HasForeignKey("CourseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Attendance.Domain.Entities.Group", "Group")
                        .WithMany("CourseGroups")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Presence", b =>
                {
                    b.HasOne("Attendance.Domain.Entities.Course", "Course")
                        .WithMany()
                        .HasForeignKey("CourseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Student", b =>
                {
                    b.HasOne("Attendance.Domain.Entities.Group", "Group")
                        .WithMany("Students")
                        .HasForeignKey("GroupId");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.StudentPresence", b =>
                {
                    b.HasOne("Attendance.Domain.Entities.Presence", "Presence")
                        .WithMany("StudentPresences")
                        .HasForeignKey("PresenceId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Attendance.Domain.Entities.Student", "Student")
                        .WithMany("StudentPresences")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
