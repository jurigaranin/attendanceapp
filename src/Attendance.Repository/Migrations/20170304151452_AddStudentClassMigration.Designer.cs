﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Attendance.Repository.EF;

namespace Attendance.Repository.Migrations
{
    [DbContext(typeof(AtendanceContext))]
    [Migration("20170304151452_AddStudentClassMigration")]
    partial class AddStudentClassMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Attendance.Domain.Entities.Admin", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Login");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.ToTable("Admins");
                });

            modelBuilder.Entity("Attendance.Domain.Entities.Student", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("FirstName");

                    b.Property<string>("Lastname");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.ToTable("Students");
                });
        }
    }
}
