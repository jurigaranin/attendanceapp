﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.Domain.Entities;
using Attendance.Repository.Interfaces;
using Attendance.Repository.EF;
using Microsoft.EntityFrameworkCore;

namespace Attendance.Repository.Repositories
{
    public class GroupRepository : IRepository<Group>
    {
        private AtendanceContext db;

        public GroupRepository(AtendanceContext context)
        {
            this.db = context;
        }

        public IEnumerable<Group> GetAll()
        {
            return db.Groups;         
        }

        public void Create(Group group)
        {
            db.Groups.Add(group);
            db.SaveChanges();
        }

        public void Update(Group group)
        {
            db.Entry(group).State = EntityState.Modified;
            db.SaveChanges();

        }

        public void Delete(Group group)
        {

            db.Groups.Remove(group);
            db.SaveChanges();

        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}
