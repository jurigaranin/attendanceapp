﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.Domain.Entities;
using Attendance.Repository.Interfaces;
using Attendance.Repository.EF;
using Microsoft.EntityFrameworkCore;

namespace Attendance.Repository.Repositories
{
    public class AdminRepository : IRepository<Admin>
    {
        private AtendanceContext db;

        public AdminRepository(AtendanceContext context)
        {
            this.db = context;
        }

        public IEnumerable<Admin> GetAll()
        {
            return db.Admins;

        }

        public void Create(Admin admin)
        {
            db.Admins.Add(admin);
            db.SaveChanges();
        }

        public void Update(Admin admin)
        {
            db.Entry(admin).State = EntityState.Modified;
            db.SaveChanges();

        }

        public void Delete(Admin admin)
        {

            db.Admins.Remove(admin);
            db.SaveChanges();

        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

    }
}
