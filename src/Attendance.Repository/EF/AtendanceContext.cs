﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Attendance.Domain.Entities;

namespace Attendance.Repository.EF
{
    public class AtendanceContext : DbContext
    {
        
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Presence> Presences { get; set; }

        public AtendanceContext(DbContextOptions<AtendanceContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CourseGroup>()
                .HasKey(t => new { t.CourseId, t.GroupId });

            modelBuilder.Entity<CourseGroup>()
                .HasOne(sc => sc.Course)
                .WithMany(s => s.CourseGroups)
                .HasForeignKey(sc => sc.CourseId);

            modelBuilder.Entity<CourseGroup>()
                .HasOne(sc => sc.Group)
                .WithMany(c => c.CourseGroups)
                .HasForeignKey(sc => sc.GroupId);


            modelBuilder.Entity<StudentPresence>()
                .HasKey(t => new { t.StudentId, t.PresenceId });

            modelBuilder.Entity<StudentPresence>()
                .HasOne(sc => sc.Student)
                .WithMany(s => s.StudentPresences)
                .HasForeignKey(sc => sc.StudentId);

            modelBuilder.Entity<StudentPresence>()
                .HasOne(sc => sc.Presence)
                .WithMany(c => c.StudentPresences)
                .HasForeignKey(sc => sc.PresenceId);
        }

    }
}
