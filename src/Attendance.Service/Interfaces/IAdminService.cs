﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.Repository.EF;
using Attendance.Domain.Entities;

namespace Attendance.Service.Interfaces
{
    public interface IAdminService
    {
        IEnumerable<Admin> GetAdmins();
        void CreateAdmin(Admin admin);
        void UpdateAdmin(Admin admin);
        void DeleteAdmin(Admin admin);
    }
}
