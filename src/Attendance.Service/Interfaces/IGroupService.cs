﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.Repository.EF;
using Attendance.Domain.Entities;

namespace Attendance.Service.Interfaces
{
    public interface IGroupService
    {
        IEnumerable<Group> GetGroups();
        void CreateGroup(Group group);
        void UpdateGroup(Group group);
        void DeleteGroup(Group group);
    }
}
