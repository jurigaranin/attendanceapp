﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.Service.Interfaces;
using Attendance.Repository.Interfaces;
using Attendance.Repository.Repositories;
using Attendance.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Attendance.Service.Services
{
    public class AdminService : IAdminService
    {
        private IRepository<Admin> adminRepository;

        public AdminService(IRepository<Admin> adminRepository)
        {
            this.adminRepository = adminRepository;
        }

        public IEnumerable<Admin> GetAdmins()
        {
            return adminRepository.GetAll();
        }

        public void CreateAdmin(Admin admin)
        {
            adminRepository.Create(admin);
        }

        public void UpdateAdmin(Admin admin)
        {
            adminRepository.Update(admin);
        }

        public void DeleteAdmin(Admin admin)
        {
            adminRepository.Delete(admin);
        }
    }
}
