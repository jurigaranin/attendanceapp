﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.Service.Interfaces;
using Attendance.Repository.Interfaces;
using Attendance.Repository.Repositories;
using Attendance.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Attendance.Service.Services
{
    public class GroupService :IGroupService
    {
        private IRepository<Group> groupRepository;

        public GroupService(IRepository<Group> groupRepository)
        {
            this.groupRepository = groupRepository;
        }

        public IEnumerable<Group> GetGroups()
        {
            return groupRepository.GetAll();
        }

        public void CreateGroup(Group group)
        {
            groupRepository.Create(group);
        }

        public void UpdateGroup(Group group)
        {
            groupRepository.Update(group);
        }

        public void DeleteGroup(Group group)
        {
            groupRepository.Delete(group);
        }
    }
}
