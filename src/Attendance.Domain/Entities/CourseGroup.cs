﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Domain.Entities
{
    public class CourseGroup
    {
        public Guid CourseId { get; set; }
        public Course Course { get; set; }

        public string GroupId { get; set; }
        public Group Group { get; set; }
    }
}
