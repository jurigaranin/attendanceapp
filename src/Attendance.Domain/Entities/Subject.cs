﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Domain.Entities
{
    public class Subject
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Eap { get; set; }
        public int Practice { get; set; }
        public int Lesson { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public Subject()
        {
            Courses = new List<Course>();
        }
    }
}
