﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Domain.Entities
{
    public class StudentPresence
    {
        public string StudentId { get; set; }
        public Student Student { get; set; }

        public Guid PresenceId { get; set; }
        public Presence Presence { get; set; }
    }
}
