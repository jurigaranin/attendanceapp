﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Domain.Entities
{
    public class Student
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }

        public string GroupId { get; set; }
        public Group Group { get; set; }

        public virtual ICollection<StudentPresence> StudentPresences { get; set; }

        public Student()
        {
            StudentPresences = new List<StudentPresence>();
        }
    }
}
