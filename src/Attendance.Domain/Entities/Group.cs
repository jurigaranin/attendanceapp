﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Domain.Entities
{
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<CourseGroup> CourseGroups { get; set; }

        public Group()
        {
            Students = new List<Student>();
            CourseGroups = new List<CourseGroup>();   
        }
    }
}
