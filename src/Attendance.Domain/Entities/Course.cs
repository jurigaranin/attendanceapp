﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Domain.Entities
{
    public class Course
    {
        public Guid Id { get; set; }

        public string SubjectId { get; set; }
        public Subject Subject { get; set; }

        public string TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public virtual ICollection<CourseGroup> CourseGroups { get; set; }

        public char CourseType { get; set; }

        public Course()
        {
            CourseGroups = new List<CourseGroup>();
        }
    }
}
